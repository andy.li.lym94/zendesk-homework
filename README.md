## Zendesk-homework

## Data files:
1. tickets.json
2. user.json
3. organisation.json

## Requirements
1. Command-line script
2. Search data, any field, exact value matching (empty values also)
3. Return results in human readable format

## Installation requirements
1. Run `docker build -t zen-search .`
2. Once build completed run `docker run -t -i zen-search`

## Usage
1. Select the file to search from
2. Select the field to search from
3. Dependent on the search field, you'll need to enter the search parameter in different ways, you will be asked to confirm input.
4. Search result is returned and you have the choice of displays. [Print All] or [One by One]. If search returns no result, you'll be asked if you want to search again.
5. Print all: will print all of the files at once, One by One will print the files and give user control to navigate the results.
6. Once done, you will be asked if you want to search again or exit.
7. Search again will loop back to step 1
## Issues/limitations
1. Currently, the interface will break if focus is lost on the terminal. 
2. Currently the itnerface will break if clicked on, issue with PyInquirer.
3. Tags search only work in 'any' mode.

## Assumptions
## Schema
This program's search functionality is heavily dependent on data types. Generally speaking, an application like such will require a schema to be known and therefore making that assumption. Schema inference becomes complicated, particular as the file gets bigger.
## Testing
Testing for a user-interface can be done, but neglected for this particular exercise. Testing is only conduct on the search functions.
## Human readability
It was assumed that JSON isn't human (normal) readeable.
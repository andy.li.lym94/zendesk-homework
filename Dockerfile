FROM python:3.7-slim-buster

COPY . .

RUN pip3 install prompt_toolkit==1.0.14
RUN pip3 install schema==0.7.4
RUN pip3 install PyInquirer==1.0.3

ENTRYPOINT [ "python3", "source/zen_search.py" ]
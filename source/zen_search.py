import os, json

from utilities import user_interaction
from schema_file import file_schemas
from search import field_search
from display_results import print_result


DATA_DIR = "./data/"


def zen_search(data_directory):
    os.system("cls||clear")
    # reads potential list of searcheable files
    files_list = os.listdir(data_directory)
    file_choice = user_interaction(
        "list", "files_choice", "Which file would you like to search?", None, files_list
    )

    # reads file & schema
    with open(f"{data_directory}{file_choice}") as f:
        file_content = json.load(f)
    file_schema = file_schemas.get(file_choice)

    # get metadata:
    field_choice = user_interaction(
        "list",
        "field_choice",
        "Which field would you like to search on?",
        "",
        file_schema.schema.keys(),
    )
    field_dtype = file_schema.schema.get(field_choice)

    # main switch function to handle user interaction of different types.
    SEARCH_PROMPT = f"Search field: '{field_choice}', what is your search input?"

    if field_dtype == str:
        search_input = user_interaction(
            "input",
            "search_input",
            SEARCH_PROMPT,
            field_dtype,
            user_confirm=True,
        )
    elif field_dtype == list:
        tags = []
        for file in file_content:
            tags += file.get(field_choice)
        search_input = user_interaction(
            "checkbox",
            "search_input",
            SEARCH_PROMPT,
            field_dtype,
            [{"name": tag} for tag in sorted(set(tags))],
            user_confirm=True,
        )
    elif field_dtype == bool:
        search_input = user_interaction(
            "list",
            "search_input",
            SEARCH_PROMPT,
            field_dtype,
            ["true", "false", "unfilled"],
            user_confirm=True,
        )
    elif field_dtype == int:
        search_input = user_interaction(
            "input",
            "search_input",
            SEARCH_PROMPT,
            field_dtype,
            user_confirm=True,
        )
    else:
        print("Field type unimpleted")

    # searching
    search_result = field_search(file_content, field_choice, field_dtype, search_input)

    # display results
    if len(search_result) == 0:
        print("Your search returned 0 results.")
    else:
        print_result(search_result)
    
    # search again or exit.
    exit_result = False
    exit_result = user_interaction(
        "confirmation",
        "search_input",
        "Do you wish to search again? (n) will exit the program.",
        "",
    )
    return (
        print("Exiting program") if exit_result == False else zen_search(data_directory)
    )


if __name__ == "__main__":
    zen_search(DATA_DIR)

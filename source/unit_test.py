from search import field_search


# int test
int_test_input = [
    {"input": 101, "output": True},
    {"input": 102, "output": False},
    {"input": -101, "output": False},
    {"input": "101", "output": False},
    {"input": 101.0, "output": False},
]

int_test_output = field_search(int_test_input, "input", int, 101)
assert len(int_test_output) == 1
assert int_test_output[0].get("output")

# str test
str_test_input = [
    {"input": "test", "output": True},
    {"input": "", "output": False},
    {"input": "negative-test", "output": False},
    {"input": None, "output": False},
]

str_test_output = field_search(str_test_input, "input", str, "test")
assert len(str_test_output) == 1
assert str_test_output[0].get("output")

# null str test
str_test_input = [
    {"input": "test", "output": False},
    {"input": "", "output": True},
    {"input": "negative-test", "output": False},
    {"input": None, "output": False},
]
null_str_test_output = field_search(str_test_input, "input", str, "")
assert len(null_str_test_output) == 1
assert null_str_test_output[0].get("output")

# bool str test
bool_test_input = [
    {"input": True, "output": True},
    {"input": False, "output": False},
    {"input": None, "output": False},
]

bool_test_output = field_search(bool_test_input, "input", bool, "true")
assert len(bool_test_output) == 1
assert bool_test_output[0].get("output")

# list test
list_test_input = [
    {
        "input": ["a", "b", "c", "d"],
    },
    {
        "input": ["a"],
    },
    {
        "input": ["x", "c", "e"],
    },
    {
        "input": ["b", "c", "d"],
    },
    {
        "input": [],
    },
]

list_test_output = field_search(list_test_input, "input", list, ["a"])
assert len(list_test_output) == 2

list_test_output = field_search(list_test_input, "input", list, [])
assert len(list_test_output) == 1

list_test_output = field_search(list_test_input, "input", list, ["e", "x"])
assert len(list_test_output) == 1

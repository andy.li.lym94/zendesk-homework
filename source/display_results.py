import os
import pprint

from utilities import user_interaction


class bcolors:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


PRETTY_PRINTER = pprint.PrettyPrinter(indent=4)


def print_result(search_results):
    if len(search_results) > 1:
        print(f"Search returned {len(search_results)} results.")
        multi_display(search_results)
    else:
        for key, value in search_results[0].items():
            print(f"{bcolors.OKBLUE}{key}{bcolors.ENDC}: {value}")


def multi_display(search_results):
    user_input = user_interaction(
        "list",
        "display_choices",
        f"There are {len(search_results)} results. How would to display?",
        "",
        ["Display All", "One by One", "Exit"],
    )

    if user_input == "Display All":
        for i, result in enumerate(search_results):
            print("-" * 30, f"Item Number {i+1}", "-" * 30)
            for key, value in result.items():
                print(f"{bcolors.OKBLUE}{key}{bcolors.ENDC}: {value}")
    elif user_input == "One by One":
        page_control(search_results, 0)
    else:
        return


def page_control(search_results, item_number):
    os.system('cls||clear')
    if item_number == 0:
        choices = ["Next", "Exit"]
    elif item_number == len(search_results) - 1:
        choices = ["Previous", "Exit"]
    else:
        choices = ["Previous", "Next", "Exit"]

    for key, value in search_results[item_number].items():
        print(f"{bcolors.OKBLUE}{key}{bcolors.ENDC}: {value}")

    user_input = user_interaction(
        "list",
        "display_choices",
        f"Displaying result number: {item_number+1}/{len(search_results)}",
        "",
        choices,
    )

    if user_input == "Previous":
        item_number -= 1
    elif user_input == "Next":
        item_number += 1
    else:
        return
    return page_control(search_results, item_number)

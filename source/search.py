def field_search(file_content, field_choice, field_dtype, search_input):
    search_output = []
    print(f"Initiating search on '{field_choice}', against '{search_input}'")

    if field_dtype == str:
        for row in file_content:
            if row.get(field_choice) == search_input:
                search_output.append(row)

    elif field_dtype == list:
        if search_input == []:
            for row in file_content:
                if row.get(field_choice) == []:
                    search_output.append(row)
        else:
            for row in file_content:
                if any(item in search_input for item in row.get(field_choice)):
                    search_output.append(row)

    elif field_dtype == bool:
        if search_input == "true":
            search_input = True
        elif search_input == "false":
            search_input = False
        else:
            search_input = ""

        for row in file_content:
            if row.get(field_choice) == search_input:
                search_output.append(row)

    elif field_dtype == int:
        if search_input != "":
            for row in file_content:
                if (
                    row.get(field_choice) == int(search_input)
                    and type(row.get(field_choice)) == int
                ):
                    search_output.append(row)
        else:
            for row in file_content:
                if row.get(field_choice) in [search_input, None, ""]:
                    search_output.append(row)
    return search_output

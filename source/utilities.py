from PyInquirer import prompt
from prompt_toolkit.validation import Validator, ValidationError


def user_interaction(
    choice_type, choice_name, message, expected_dtype, choices=[], user_confirm=False
):
    if expected_dtype == int:
        validator = IntegerValidator
    else:
        validator = None

    if choice_type == "list":
        user_input = prompt(
            [
                {
                    "type": "list",
                    "name": choice_name,
                    "message": message,
                    "choices": choices,
                }
            ]
        ).get(choice_name)
    elif choice_type == "input":
        user_input = prompt(
            [
                {
                    "type": "input",
                    "name": choice_name,
                    "message": message,
                    "default": "",
                    "validate": validator,
                }
            ]
        ).get(choice_name)
    elif choice_type == "checkbox":
        user_input = prompt(
            [
                {
                    "type": "checkbox",
                    "name": choice_name,
                    "message": message,
                    "choices": choices,
                }
            ]
        ).get(choice_name)
    elif choice_type == "confirmation":
        user_input = prompt(
            [
                {
                    "type": "confirm",
                    "name": choice_name,
                    "message": message,
                    "default": True,
                }
            ]
        ).get(choice_name)
    else:
        print(
            "Interaction type unimplemented, please try 'list', 'input' or 'checkbox'."
        )

    if user_confirm:
        return (
            user_input
            if confirmation(user_input)
            else user_interaction(
                choice_type, choice_name, message, choices, user_confirm
            )
        )
    else:
        return user_input


def confirmation(input):
    user_input = prompt(
        [
            {
                "type": "confirm",
                "name": "confirmation",
                "message": f"Your input is '{input}'. Confirm?",
                "default": True,
            }
        ]
    ).get("confirmation")
    return user_input


class IntegerValidator(Validator):
    def validate(self, document):
        if document.text not in [None, ""]:
            try:
                int(document.text)
            except:
                raise ValidationError(
                    message="Please enter a valid integer.",
                    cursor_position=len(document.text),
                )
